$(document).ready(function () {
	'use strict';
	$(".search-btn a").click(function () {
		$(".site-search").toggleClass("toggled");
	});
	// Magnific popup
	//-----------------------------------------------
	if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) {
		$(".popup-img").magnificPopup({
			type: "image",
			gallery: {
				enabled: true,
			}
		});
		$(".popup-img-single").magnificPopup({
			type: "image",
			gallery: {
				enabled: false,
			}
		});
		$('.popup-iframe').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			preloader: false,
			fixedContentPos: false,
			mainClass: 'mfp-fade',
			removalDelay: 160,			
		});
	}


});
